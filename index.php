<pre>
<?php
//$rows = 4;
//$cols = 4;
//
//for ($r = 0; $r <= $rows; $r++) {
//    for ($c = 0; $c <= $cols; $c++) {
//        $map[$r][$c] = rand()&1;
//    }
//}
//
//$start = 'S';
//$arrival = 'A';
//
//$map[0][0] = $start;
//$map[4][4] = $arrival;
//
//foreach ($map as $key => $val) {
//    foreach ($val as $k => $v) {
//        echo ' '. $v;
//    }
//    echo PHP_EOL;
//}

$map = [
    [0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1],
    [0, 0, 0, 0, 1],
    [0, 1, 1, 1, 1],
    [0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0],
];

foreach ($map as $key => $val) {
    foreach ($val as $k => $v) {
        echo ' '. $v;
    }
    echo PHP_EOL;
}

$start = $map[0][0];
$arrival = $map[5][4];

$possibleWaysList = [];
$longestStepsNumber = count($map)*count($map[0]);
//i rows
//j cols
$i = 0;
$j = 0;

function findBestNode($i, $j){
    //TODO

}

while ($i != count($map) && $j != count($map[0]) && count($possibleWaysList) > 0) {
    $stepsNumber = 1;
    $possibilitiesNumber = 0;
    $possibilities = [];

    if ($map[$i][$j + 1] == 0) {
        // Possibilité d'aller à droite
        $possibilitiesNumber++;
        array_push($possibleWaysList, ['row' => $i, 'col' => $j+1, 'stepsNumber' => $stepsNumber]);
    }

    if ($j > 0 && $map[$i][$j - 1] == 0) {
        // Possibilité d'aller à gauche
        $possibilitiesNumber++;
        array_push($possibleWaysList, ['row' => $i, 'col' => $j-1, 'stepsNumber' => $stepsNumber]);
    }

    if ($map[$i+1][$j] == 0) {
        // Possibilité d'aller en dessous
        $possibilitiesNumber++;
        array_push($possibleWaysList, ['row' => $i+1, 'col' => $j, 'stepsNumber' => $stepsNumber]);
    }

    if ($i > 0 && $map[$i-1][$j] == 0) {
        // Possibilité d'aller en haut
        $possibilitiesNumber++;
        array_push($possibleWaysList, ['row' => $i-1, 'col' => $j, 'stepsNumber' => $stepsNumber]);
    }

    // check quand on est bloqué ou en boucle infini
    if ($possibilitiesNumber == 1 || $stepsNumber >= $longestStepsNumber) {
        $lastPossibility = array_pop($possibleWaysList);
        $i = $lastPossibility['i'];
        $j = $lastPossibility['j'];
        $stepsNumber = $lastPossibility['stepsNumber'];
    }

    if ($possibilitiesNumber > 1) {
        $nextWay = findBestNode($i, $j); // retourner un tableau [ 'i' => int, 'j' => int ] qui va déterminer quel i et quel j est plus proche de la sortie à vol d'oiseau par exemple
        // J'avance d'une case
        $i = $nextWay['i'];
        $j = $nextWay['j'];
        $stepsNumber++;

        // J'empile les possiblités non testées pour plus tard
        foreach($possibilities as $possibility) {
            if ($possibility['i'] != $i && $possibility['j'] != $j) {
                array_push($possibleWaysList, $possibility);
            }
        }
    }
}