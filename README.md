# Shortest path algorithm



## Description

shortest path finder ( visually maze-like )

S 0 0 0 1 <br>
1 0 1 0 1 <br>
0 0 0 0 1 <br>
0 1 1 1 1 <br>
0 0 0 1 1 <br>
1 1 0 0 A <br>

- 1 are walls
- 0 are path possibilities
- Start is first index [0][0]
- Arrival is [5][4]

######written in PHP

## Run the project

```
git clone git@gitlab.com:jordybastide/shortestpath_algo.git
cd shortestpath_algo
php index.php
```

or go to :

http://localhost/shortestpath_algo/index.php

if you run it locally
